// ? = Optional property
export class User {
    username?: string;
    password?: string;
    jwt?: string;
}

