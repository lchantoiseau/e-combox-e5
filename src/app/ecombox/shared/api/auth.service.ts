import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class AuthService extends ApiService {
  /**
   * @param {number} id
   * @param {string} newPasswd
   * @param {string} currentPasswd
   * @return {Observable<void>}
   */
  public changePasswd(
    id: number,
    newPasswd: string,
    currentPasswd: string
  ): Observable<void> {
    return this.httpClient.put<void>(`${this.basePath}/users/${id}/passwd`, {
      newPassword: newPasswd,
      password: currentPasswd,
    });
  }
}
