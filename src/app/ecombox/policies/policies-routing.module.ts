/**
 *       {
        path: "policies",
        component: PoliciesComponent,
      },
      {
        path: "edit-policies",
        component: FormPoliciesComponent,
      },
 */
import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { PoliciesComponent } from "./policies.component";
import { FormPoliciesComponent } from "./form-policies/form-policies.component";

const routes: Routes = [
  { path: "", component: PoliciesComponent },
  { path: "edit", component: FormPoliciesComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PoliciesRoutingModule {}
