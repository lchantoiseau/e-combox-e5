import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { ThemeModule } from "../../@theme/theme.module";
import { TranslateModule } from "@ngx-translate/core";
import {
  NbCardModule,
  NbRadioModule,
  NbInputModule,
  NbButtonModule,
  NbSpinnerModule,
  NbPopoverModule,
  NbIconModule,
  NbTooltipModule,
  NbContextMenuModule,
  NbSelectModule,
  NbToggleModule,
  NbButtonGroupModule,
  NbCheckboxModule,
  NbAlertModule,
} from "@nebular/theme";
import { NbEvaIconsModule } from "@nebular/eva-icons";
import { ServersRoutingModule } from "./servers-routing.module";
import { NbDialogModule } from "@nebular/theme";
import { ServersCreateComponent } from "./server-create/servers-create.component";
import { ServerBaseComponent } from "./server-base/server-base.component";
import { ServerAdvancedComponent } from "./server-advanced/server-advanced.component";
import { FormsModule } from "@angular/forms";
import { ServersManageComponent } from "./server-manage/servers-manage.component";
import { ServerStatusCardComponent } from "./server-status-card/server-status-card.component";
import { ServerAdvancedCardComponent } from "./server-advanced-card/server-advanced-card.component";
import { DialogNamePromptComponent } from "./dialog-name-prompt/dialog-name-prompt.component";
import { ShowcaseDialogComponent } from "./dialog-pdf/showcase-dialog.component";
import { DialogTemplateComponent } from "./dialog-template/dialog-template.component";
import { DialogMultipleStacksPromptComponent } from "./dialog-multiple-stacks-prompt/dialog-multiple-stacks-prompt.component";
import { ServerDefaultLoginCredentialsComponent } from "./server-default-login-credentials/server-default-login-credentials.component";

/**
 * Sources of the “servers” module - Contains all components related to Docker containers.
 */
@NgModule({
  imports: [
    ThemeModule,
    NbCardModule,
    NbRadioModule,
    NbInputModule,
    NbButtonModule,
    NbPopoverModule,
    FormsModule,
    NbButtonGroupModule,
    ServersRoutingModule,
    NbSpinnerModule,
    NbIconModule,
    NbEvaIconsModule,
    NbAlertModule,
    NbTooltipModule,
    NbContextMenuModule,
    NbCheckboxModule,
    NbSelectModule,
    ReactiveFormsModule,
    NbToggleModule,
    TranslateModule.forChild({
      extend: true,
    }),
    NbDialogModule.forChild(),
  ],
  declarations: [
    ServersCreateComponent,
    ServerBaseComponent,
    ServersManageComponent,
    ServerStatusCardComponent,
    DialogNamePromptComponent,
    DialogMultipleStacksPromptComponent,
    ShowcaseDialogComponent,
    ServerAdvancedComponent,
    ServerAdvancedCardComponent,
    DialogTemplateComponent,
    ServerDefaultLoginCredentialsComponent,
  ],
})
export class ServersModule {}
