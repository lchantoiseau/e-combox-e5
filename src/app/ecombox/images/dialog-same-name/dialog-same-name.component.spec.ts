import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogSameNameComponent } from './dialog-same-name.component';

describe('DialogSameNameComponent', () => {
  let component: DialogSameNameComponent;
  let fixture: ComponentFixture<DialogSameNameComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSameNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSameNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
