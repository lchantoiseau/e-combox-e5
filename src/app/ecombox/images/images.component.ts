/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { Component } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { DockerRegistryService } from "../shared/api/docker-registry.service";
import { StackStore } from "../shared/store/stack.store";
import { HttpClient } from "@angular/common/http";
import { EXPORT_SITE } from "../shared/api/variables";
import { Router } from "@angular/router";
import { ImageService } from "../shared/api/image.service";
import { map, mergeMap, catchError } from "rxjs/operators";
import { EMPTY } from "rxjs";
import { LocalDataSource } from "angular2-smart-table";
import { NbDialogService } from "@nebular/theme";
import { DialogNamePromptComponent } from "../servers/dialog-name-prompt/dialog-name-prompt.component";

@Component({
  selector: "ecx-images",
  styleUrls: ["./images.component.scss"],
  templateUrl: "./images.component.html",
})
/**
 * Sources of the “images” module. Displays the list of images created by users.
 */
export class ImagesComponent {
  public source = new LocalDataSource();

  settings = {
    hideSubHeader: true,
    actions: {
      custom: [
        {
          name: "share",
          title:
            '<i class="ion-upload" id="att" title="Partager le modèle"></i>',
        },
      ],
      add: false,
    },
    rowClassFunction: (row) => {
      return row.data.type;
    },
    edit: {
      editButtonContent:
        '<i class="nb-edit" title="Changer le nom du modèle"></i>',
      saveButtonContent:
        '<i class="ion-checkmark-round" title="Changer le nom du modèle"></i>',
      cancelButtonContent:
        '<i class="ion-ios-undo-outline" title="Annuler"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent:
        '<i class="nb-trash" title="Supprimer le modèle"></i>',
      confirmDelete: true,
    },
    columns: {
      tag: {
        title: "Nom du modèle",
      },
      type: {
        title: "Type de modèle de site",
        editable: false,
      },
    },
  };

  data = [];

  /**
   *
   * @param {DockerRegistryService} registryService
   * @param {HttpClient} httpClient
   */
  constructor(
    private registryService: DockerRegistryService,
    private store: StackStore,
    private toastr: ToastrService,
    protected httpClient: HttpClient,
    private router: Router,
    private imageService: ImageService,
    private dialogService: NbDialogService,
  ) {
    const [domain, port] = this.imageService.urlBasePath();

    this.registryService.getAllTags(domain, port).subscribe((result) => {
      result.forEach((repo) => {
        if (repo.tags) {
          repo.tags.forEach((tag: string) => {
            this.data.push({
              tag: tag,
              type: repo.name,
            });
          });
        }
      });
      this.source.load(this.data);
    });

    this.imageService.subject.subscribe((res) => {
      this.data.push({
        tag: res[0],
        type: res[1],
      });
      this.source.load(this.data);
    });
  }

  /**
   *
   * @param {Event} event
   */
  onDeleteConfirm(event): void {
    // console.log(event.data.tag + " != " + event.newData.tag);
    this.dialogService
      .open(DialogNamePromptComponent, {
        context: {
          message: "Cette action supprimera définitivement ce modèle",
        },
      })
      .onClose.subscribe((confirmDelete) => {
        if (confirmDelete !== undefined) {
          const [domain, port] = this.imageService.urlBasePath();

          this.registryService
            .registryDelImage(event.data.type, event.data.tag, domain, port)
            .subscribe(() => {
              this.toastr.success(
                "Le modèle " + event.data.tag + " a été supprimé.",
              );
            });
          event.confirm.resolve();
        } else {
          event.confirm.reject();
        }
      });
  }

  /**
   *
   * @param {Event} event
   */
  public form(event): void {
    EXPORT_SITE.siteType = event.data.type;
    EXPORT_SITE.modelName = event.data.tag;
    this.router.navigate(["ecombox/images/export"]);
  }

  /**
   *
   */
  public import(): void {
    this.router.navigate(["ecombox/images/import"]);
  }

  /**
   *
   * @param {Event} event
   */
  public onEdit(event): void {
    let dif = true;
    for (let i = 0; i < this.data.length; i++) {
      if (
        this.data[i].tag === event.newData.tag &&
        this.data[i].type === event.data.type &&
        this.data[i].tag != event.data.tag
      ) {
        this.toastr.error("Une image du même nom existe déjà");
        dif = false;
        event.confirm.reject();
        break;
      }
    }

    if (dif && event.data.tag != event.newData.tag) {
      const [domain, port] = this.imageService.urlBasePath();
      const localPort = this.store.getResgistryPort();
      const oldName = this.imageService.imageLocalName(
        localPort,
        event.data.type,
        event.data.tag,
      );
      const newName = this.imageService.imageLocalName(
        localPort,
        event.data.type,
        event.newData.tag,
      );
      const oldNamedb = this.imageService.imageLocalName(
        localPort,
        event.data.type + "-db",
        event.data.tag,
      );
      const newNamedb = this.imageService.imageLocalName(
        localPort,
        event.data.type + "-db",
        event.newData.tag,
      );

      this.imageService
        .imageShareUpDate(
          oldNamedb,
          newNamedb,
          "",
          "",
          event.data.type + "-db",
          domain,
          port,
          true,
        )
        .pipe(
          mergeMap(() =>
            this.imageService.imageShareUpDate(
              oldName,
              newName,
              "",
              "",
              event.data.type,
              domain,
              port,
              true,
            ),
          ),
          map(() => {
            event.confirm.resolve();
            this.toastr.success("Succès du changement de nom");
          }),
          catchError(() => {
            event.confirm.reject();
            this.toastr.error("Echec du changement de nom");
            return EMPTY;
          }),
        )
        .subscribe();
    } else if (dif && event.data.tag === event.newData.tag) {
      event.confirm.resolve();
    }
  }
}
