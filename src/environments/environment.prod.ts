/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import packageInfo from "../../package.json";

export const environment = {
  production: true,
  ecomboxVersion: packageInfo.version,
};
