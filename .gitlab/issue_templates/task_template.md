## Description

(Détails de la tâche à réaliser)

## Catégorie

(Choisir une ou plusieurs catégories en utilisant la syntaxe suivante : `/label ~category`)

## Temps prévu

(Préciser ici le temps prévu en utilisant la syntaxe suivante : )

## Temps passé

(Préciser ici le temps prévu en utilisant la syntaxe suivante : )

## Références
(Référencer ici les liens vers d'autres tâches en utilisant la syntaxe suivante : )
